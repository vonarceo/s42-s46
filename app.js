const express = require('express')
const dotenv = require('dotenv')
const mongoose = require('mongoose')
const cors = require('cors')
const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')

dotenv.config()

const app = express()
const port = process.env.PORT || 4000;

//MongoDB Connection

mongoose.connect(`mongodb+srv://vonarceo:${process.env.PASSWORD}@cluster0.x7hwzcj.mongodb.net/capstone2?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection
db.once('open', () => console.log('Connected to mongoDB!'))

//MongoDB Connection End

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}))


//routes
app.use('/users', userRoutes)
app.use('/products', productRoutes)
//routes end




app.listen(port, () => {
	console.log(`API is now running on localhost:${port}`)
})