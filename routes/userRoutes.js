const express = require('express')
const router = express.Router()
const UserController = require('../controllers/UserController')
const auth = require('../auth')


// Register User
router.post('/register', (request, response) => {
	UserController.register(request.body).then((result) => {
		response.send(result)
	})
})

// List of all Users
router.get('/list', (request, response) => {
	UserController.getAllUsers().then((result) => {
		response.send(result)
	})
})
//login 
router.post('/login', (request, response) => {
	UserController.login(request.body).then((result) => {
		response.send(result)
	})
})

router.get('/:id/details', auth.verify, (request, response) => {
	UserController.getUserDetails(request.params.id).then((result) => {
		response.send(result)
	})
})

//list of admin

router.get('/admin', (request, response) => {
	UserController.getAllAdmin().then((result) => {
		response.send(result)
	})
})

// order check out

router.post('/checkout', auth.verify, (request, response) => {
	let data = {
		userId: request.body.userId,
		productId: request.body.productId,
		productName: request.body.productName,
		quantity: request.body.quantity,
		totalAmount: request.body.totalAmount
		
	}

	UserController.checkOut(data).then((result) => {
		response.send(result)
	})
})


router.patch('/:userId/promoteuser', auth.verify, (request, response) => {
	const data = {
		userId : request.params.userId,
		password: request.body.password,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	UserController.promoteUser(data).then((result) => {
		response.send(result)
	})

})

router.delete('/:userId/deleteuser', auth.verify, (request, response) => {
	const data = {
		userId: request.params.userId,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	UserController.deleteUser(data).then((result) => {
		response.send(result)
	})

})


// retrieve authenticated users order
router.get('/:userId/getorder', auth.verify, (request, response) => {
	UserController.getOrders(request.params.userId).then((result) => {
		response.send(result)
	})
})


router.get('/getAllOrders', auth.verify, (request, response) => {
	const data = {
		
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	UserController.getAllOrders(data).then((result) => {
		response.send(result)
	})

})

// cart

router.post('/cart', auth.verify, (request, response) => {
	const data = {
		cartProduct: request.body
	}

	UserController.userCart(data).then((result) => {
		response.send(result)
	})
})

// view cart

router.post('/viewcart', auth.verify, (request, response) => {
	UserController.viewCart(request.body).then((result) => {
		response.send(result)
	})
})

// test data

router.post('/testtest', (request, response) => {
	UserController.testTest(request.body).then((result) => {
		response.send(result)
	})
})



module.exports = router
