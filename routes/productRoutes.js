const express = require('express')
const router = express.Router()
const ProductController = require('../controllers/ProductController')
const auth = require('../auth')

// create/post new product
router.post('/create', auth.verify, (request, response) => {
	const data = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	ProductController.addProduct(data).then((result) => {
		response.send(result)
	})
})
// get a list of all active products
router.get('/active', (request, response) => {
	ProductController.getAllActive().then((result) => {
		response.send(result)
	})
})
//get all product with users ordered
router.get('/', (request, response) => {
	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	ProductController.getAllProduct(data).then((result) => {
		response.send(result)
	})
})

// get a specific products
router.get('/:productId', (request, response) => {
	ProductController.getProduct(request.params.productId).then((result) => {
		response.send(result)
	})
})


// Update a product if Admin
router.patch('/:productId/update', auth.verify, (request, response) => {
	const data = {
		productId : request.params.productId,
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	ProductController.updateProduct(data).then((result) => {
		response.send(result)
	})

})



// archive product with admin
router.patch('/:productId/archive', auth.verify, (request, response) => {
	const data = {
		productId: request.params.productId,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	ProductController.archiveProduct(data).then((result) => {
		response.send(result)
	})

})

// delete specific product

router.delete('/:productId', auth.verify, (request, response) => {
	const data = {
		productId: request.params.productId,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	ProductController.deleteProduct(data).then((result) => {
		response.send(result)
	})

})
//delete all product
router.delete('/', auth.verify, (request, response) => {
	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	
	ProductController.deleteAll(data).then((result) => {
	response.send(result)
	})

})
// get specific orders from a single product
router.get('/:productId/ordered', auth.verify, (request, response) => {

	const data = {	

		productId: request.params.productId,
		isAdmin: auth.decode(request.headers.authorization).isAdmin

	}

	ProductController.getorder(data).then((result) => {
		response.send(result)
	})
})




module.exports = router

