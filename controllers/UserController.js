const User = require('../models/User')
const Product = require('../models/Product')
const Cart = require('../models/Cart')
const auth = require('../auth')
const bcrypt = require('bcrypt')


//Register User w/ validation

module.exports.register = (data) => {
	return User.find({email: data.email}).then((result) => {
		if(result.length > 0){
			return {
				message: `${data.email} already exist!`
			}
		}

		else {
			let encrypted_password = bcrypt.hashSync(data.password, 10)
			let new_user = new User({
				email: data.email,
				password: encrypted_password
			})

			return new_user.save().then((created_user, error) => {
				if(error){
					return false
				}

				return {
					message: `${created_user.email} Successfully registered!'`
				}
			})			
		}
	})
}


// To View all Users
module.exports.getAllUsers = () => {
	return User.find({}, {orders: 0, password: 0, isAdmin: 0, __v: 0}).then((result) => {
		return result
	})
}

// To login 
module.exports.login = (data) => {
	return User.findOne({email: data.email}).then((result) => {
		if(result == null){
			return {
				message: "User does't exist!"
			}
		}

		const is_password_correct = bcrypt.compareSync(data.password, result.password)

		if(is_password_correct) {
			return {
				message: 'Login Successful',
				accessToken: auth.createAccessToken(result)
			}
		}

		return {

			message: 'Password is incorrect!'
		}

	})
}

// To get user details using ID
module.exports.getUserDetails = (user_id) => {
	return User.findById(user_id,{password: 0}).then((result) =>{
		//result.password = ''
		return result
	})
}

// list of admin

module.exports.getAllAdmin = () => {
	return User.find({isAdmin: true}).then((result) => {
		return result
	})
}

// order check out

module.exports.checkOut = async (data) => {

	let is_user_updated = await User.findById(data.userId).then((user) => {
		user.orders.push({
			totalAmount: data.totalAmount
		})

		user.orders[0].products.push({

			productName: data.productName,
			quantity: data.quantity
		})

		return user.save().then((updated_user, error) => {
			if(error){
				return false
			}

			return true
		})

	})

	let is_product_updated = await Product.findById(data.productId).then((product) => {
		product.orders.push({
			userId: data.userId
		})

		return product.save().then((updated_product, error) => {
			if(error){
				return false
			}

			return true
		})

	})

	if(is_user_updated && is_product_updated) {
		return {
			message: 'check-out successful'
		}

	}

	return {
		message: 'Unable to check-out, Please try again'
	}

}



// promote user to admin

module.exports.promoteUser = (data) => {
	
	if(data.isAdmin == true && data.password == 'admin'){
		return User.findByIdAndUpdate(data.userId, {
			isAdmin: true
		}).then((promote_user, error) => {
			if(error){
				return false
			}
			return {
				message: 'the user has been promoted to admin'
			}
		})
	}

	let message = Promise.resolve({
		message: 'Contact Developer to proceed with this action.'
	})

	return message.then((value) => {
		return value
	})
}
//delete specific users using admin
module.exports.deleteUser = (data) => {
	if(data.isAdmin){
		return User.findByIdAndDelete(data.userId).then((deleted_user, error) => {
			if(error){
				return false
			}
			return {
				message: `${deleted_user.email} has been deleted`
			} 
		})
	}

	let message = Promise.resolve({
		message: 'Contact Developer to proceed with this action.'
	})

	return message.then((value) => {
		return value
	})

}

// retreive authenticated user's order

module.exports.getOrders = (userId) => {
	return User.findById(userId, {email: 1, orders: 1, _id: 0}).then((result) => {
		return result
	})
}

//retreive all orders

// module.exports.getAllOrders = (data) => {


// 	return User.find({}, {_id: 0, password: 0, isAdmin: 0}).then((result) => {
// 		return result
// 	})
// }

// get all users order
module.exports.getAllOrders = (data) => {
	
	if(data.isAdmin){
		return User.find({}, {_id: 0, password: 0, isAdmin: 0}).then((result) => {
			return result
		})
	}

	let message = Promise.resolve({
		message: 'Contact Developer to proceed with this action.'
	})

	return message.then((value) => {
		return value
	})
}

// users cart

module.exports.userCart = (data) => {
	let new_cart = new Cart({
		email: data.cartProduct.email,
		productId: data.cartProduct.productId,
		productName: data.cartProduct.productName,
		quantity: data.cartProduct.quantity
	})
	return new_cart.save().then((new_cart, error) => {
		if(error){
			return false
		}
		return {
			message: `${new_cart.productName} has been added to cart`
		}
	})
}

// view cart

module.exports.viewCart = (user_email) => {
	return Cart.find({email: user_email.email},{_id: 0, email: 0, __v: 0}).then((result) => {
		return result
	})
}

// test data



module.exports.testTest = (data) => {
	return Product.find({_id: data.productId}).then((result) => {
		console.log(result)
	})
	
}
