const Product = require('../models/Product')



//create new product if admin
module.exports.addProduct = (data) => {
	if(data.isAdmin){
		let new_product = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		})
		
		return new_product.save().then((new_product, error) => {
			if(error){
				return false
			}

			return {
				message: `${new_product.name} successfully Added!`
			}
		})
	}

	let message = Promise.resolve({
		message: 'User must be ADMIN to access this.'
	})

	return message.then((value) => {
		return value
	})
}

// get all active product list
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then((result) => {
		return result
	})
}
// get all products with users ordered
module.exports.getAllProduct = (data) => {
	if(data.isAdmin){
		return Product.find({}, {name: 1, orders: 1}).then((result) => {
			return result
		})
	}
	
	let message = Promise.resolve({
		message: 'User must be ADMIN to access this.'
	})

	return message.then((value) => {
		return value
	})

	
}

// get specific product
module.exports.getProduct = (course_id) => {
	return Product.findById(course_id).then((result) => {
		return result
	})
}

// update a product if admin

module.exports.updateProduct = (data) => {
	if(data.isAdmin){
		return Product.findByIdAndUpdate(data.productId, {
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		}).then((updated_product, error) => {
			if(error){
				return false
			}
			return {
				message: `${updated_product.name} has been updated successfully`
			}
		})
	}

	let message = Promise.resolve({
		message: 'User must be ADMIN to access this.'
	})

	return message.then((value) => {
		return value
	})
}

//archive course using admin

module.exports.archiveProduct = (data) => {
	if(data.isAdmin){
		return Product.findByIdAndUpdate(data.productId, {
			isActive: false
		}).then((archive_product, error) => {
			if(error){
				return false
			}
			return {
				message: 'the product has been archived successfully'
			}
		})
	}

	let message = Promise.resolve({
		message: 'User must be ADMIN to access this.'
	})

	return message.then((value) => {
		return value
	})
}
// delete specific product

module.exports.deleteProduct = (data) => {

	if(data.isAdmin){
		return Product.findByIdAndDelete(data.productId).then((deleted_product, error) => {
			if(error){
				return false
			}

			return {
				message: `${deleted_product.name} is now deleted`
			}
		})
	}

	let message = Promise.resolve({
		message: 'User must be ADMIN to access this.'
	})

	return message.then((value) => {
		return value
	})
}

//delete all product
module.exports.deleteAll = (data) => {

	if(data.isAdmin){
		return Product.deleteMany().then((deleted_product, error) => {
			if(error){
				return false
			}

			return {
				message: `all product deleted`
			}
		})
	}

	let message = Promise.resolve({
		message: 'User must be ADMIN to access this.'
	})

	return message.then((value) => {
		return value
	})
}


// get the usersID who ordered products

module.exports.getorder = (data) => {
	if(data.isAdmin){
		return Product.findById(data.productId, {_id: 0, orders: 1}).then((result) => {
			return result
		})
	}

	let message = Promise.resolve({
		message: 'User must be ADMIN to access this.'
	})

	return message.then((value) => {
		return value
	})
}

