const mongoose = require('mongoose')

const cart_schema = new mongoose.Schema({
	email: {
		type: String,
	},
	productId: {
		type: String,
	},
	productName: {
		type: String
	},
	quantity: {
		type: Number
	},
	subTotal: {
		type: Number
	},
	totalPrice: {
		type: Number
	}

})

module.exports = mongoose.model('Cart', cart_schema)