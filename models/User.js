const mongoose = require('mongoose')

const user_schema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, 'Email is required.']
	},
	password: {
		type: String,
		required: [true, 'Password is required.']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [
	{
		products: [{
			productName: {
				type: String,
				required: [true, 'Product name is required.']
			},
			quantity: {
				type: Number,
				required: [true, 'Number of Product order is required.']
			}
		}],
		totalAmount: {
			type: Number
			
		},
		purchaseOn: {
			type: Date,
			default: new Date()
		}
	}
	]
})

module.exports = mongoose.model('User', user_schema)
